FROM debian:10.3
WORKDIR /exps
RUN apt update && apt install -y zip unzip curl wget   && \
    curl -s "https://get.sdkman.io" | bash && \
    /bin/bash -c "source /root/.sdkman/bin/sdkman-init.sh" 
COPY *.jar /exps/
ARG TAG 
RUN /bin/bash -c "source /root/.sdkman/bin/sdkman-init.sh && sdk install java $TAG" && apt install -y time 
# ENTRYPOINT ["/root/.sdkman/candidates/java/current/bin/java","-jar","dacapo-9.12-MR1-bach.jar"]