#! /bin/bash 

jvms=(
 13.0.2.j9-adpt
 13.0.2.hs-adpt
 12.0.2.j9-adpt
 12.0.2.hs-adpt
 11.0.6.j9-adpt
 11.0.6.hs-adpt
 8.0.242.j9-adpt
 8.0.242.hs-adpt
 11.0.6-amzn
 8.0.242-amzn
 13.0.2-zulu
 12.0.2-zulu
 11.0.6-zulu
 11.0.5.fx-zulu
 10.0.2-zulu
 9.0.7-zulu
 8.0.242-zulu
 8.0.232.fx-zulu
 7.0.242-zulu
 6.0.119-zulu
 13.0.2.fx-librca
 13.0.2-librca
 12.0.2-librca
 11.0.6.fx-librca
 11.0.6-librca
 8.0.242.fx-librca
 8.0.242-librca
 20.0.0.r11-grl
 20.0.0.r8-grl
 19.3.1.r11-grl
 19.3.1.r8-grl
 19.3.0.r11-grl
 19.3.0.r8-grl
 19.3.0.2.r11-grl
 19.3.0.2.r8-grl
 19.2.1-grl
 19.1.1-grl
 19.0.2-grl
 1.0.0-rc-16-grl
 15.ea.14-open
 14.ea.36-open
 12.0.2-open
 11.0.6-open
 10.0.2-open
 9.0.4-open
 8.0.242-open
 13.0.2-sapmchn
 12.0.2-sapmchn
 11.0.6-sapmchn
)

for jvm in ${jvms[@]} ; 
do 
    echo building jvm $i 
    docker build --tag chakibmed/jvm:$jvm --build-arg TAG="$jvm" -f Dockerfile .
    docker push chakibmed/jvm:$jvm
    docker pull chakibmed/jvm:$jvm
done 